#!/bin/sh

echo "This script using for create SD card to TK1 board."

if [ $# -eq 0 ] 
then
    echo "No arguments supplied... First parameter must be device root name. example: /dev/sdb"
    exit
fi

sudo umount tmp_mnt
DEV=$1
# format
sudo dd if=tk1_partition.dump of=$DEV
sudo mkfs.vfat "$DEV"1 

echo "Format finished..."

# mount
TARGET_FOLDER=tmp_mnt
mkdir -p $TARGET_FOLDER
sudo mount "$DEV"1 $TARGET_FOLDER

echo "Mount finished..."
#untar rootfs.
if [ -d rootfs/dev ]; then
    ./update.sh -m 2 -o $TARGET_FOLDER
    sudo umount $TARGET_FOLDER
else 
    echo "We dont have rootfs folder please extract it."
    echo "Example: ./create_sd_card.sh /dev/sdb rootfs_kde.tar.bz2"
    mkdir -p rootfs
    sudo tar -xf $2 -C rootfs
    ./update.sh -m 2 -o $TARGET_FOLDER
    sudo umount $TARGET_FOLDER
fi
